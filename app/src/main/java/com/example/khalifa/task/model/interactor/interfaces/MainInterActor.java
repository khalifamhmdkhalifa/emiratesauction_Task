package com.example.khalifa.task.model.interactor.interfaces;

import com.example.khalifa.task.model.response.CarsListResponse;

import io.reactivex.Observable;

public abstract class MainInterActor extends BaseNetworkInterActor {
    public abstract Observable<CarsListResponse> getCars(long ticks);
}
