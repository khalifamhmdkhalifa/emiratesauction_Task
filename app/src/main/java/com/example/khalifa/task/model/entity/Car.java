package com.example.khalifa.task.model.entity;

import com.google.gson.annotations.SerializedName;

public class Car {


    private int carId;
    private String image;
    private String descriptionAr;
    private String descriptionEn;
    private int imgCount;
    private String sharingLink;
    private String sharingMsgEn;
    private String sharingMsgAr;
    private String mileage;
    private int makeID;
    private int modelID;
    private int bodyId;
    private int year;
    private String makeEn;
    private String makeAr;
    private String modelEn;
    private String modelAr;
    private String bodyEn;
    private String bodyAr;
    private Auction AuctionInfo;

    public int getCarId() {
        return carId;
    }


    public String getImage() {
        return image;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public int getImgCount() {
        return imgCount;
    }

    public String getSharingLink() {
        return sharingLink;
    }

    public String getSharingMsgEn() {
        return sharingMsgEn;
    }

    public String getSharingMsgAr() {
        return sharingMsgAr;
    }

    public String getMileage() {
        return mileage;
    }

    public int getMakeID() {
        return makeID;
    }

    public int getModelID() {
        return modelID;
    }

    public int getBodyId() {
        return bodyId;
    }

    public int getYear() {
        return year;
    }

    public String getMakeEn() {
        return makeEn;
    }

    public String getMakeAr() {
        return makeAr;
    }

    public String getModelEn() {
        return modelEn;
    }

    public String getModelAr() {
        return modelAr;
    }

    public String getBodyEn() {
        return bodyEn;
    }

    public String getBodyAr() {
        return bodyAr;
    }

    public Auction getAuctionInfo() {
        return AuctionInfo;
    }

    public class Auction {
        private int bids;
        private long endDate;
        private String endDateEn;
        private String endDateAr;
        private String currencyEn;
        private String currencyAr;
        private int currentPrice;
        private int minIncrement;
        private int lot;
        private int priority;

        @SerializedName("VATPercent")
        private int VATPercent;
        private int isModified;
        @SerializedName("itemid")
        private int itemId;
        private int iCarId;
        private String iVinNumber;

        public int getBids() {
            return bids;
        }

        public long getEndDate() {
            return endDate;
        }

        public String getEndDateEn() {
            return endDateEn;
        }

        public String getEndDateAr() {
            return endDateAr;
        }

        public String getCurrencyEn() {
            return currencyEn;
        }

        public String getCurrencyAr() {
            return currencyAr;
        }

        public int getCurrentPrice() {
            return currentPrice;
        }

        public int getMinIncrement() {
            return minIncrement;
        }

        public int getLot() {
            return lot;
        }

        public int getPriority() {
            return priority;
        }

        public int getVATPercent() {
            return VATPercent;
        }

        public int getIsModified() {
            return isModified;
        }

        public int getItemId() {
            return itemId;
        }

        public int getiCarId() {
            return iCarId;
        }

        public String getiVinNumber() {
            return iVinNumber;
        }

        public void setEndDate(long endDate) {
            this.endDate = endDate;
        }
    }


}
