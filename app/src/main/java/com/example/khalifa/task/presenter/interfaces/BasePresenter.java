package com.example.khalifa.task.presenter.interfaces;


import com.example.khalifa.task.model.interactor.interfaces.BaseInterActor;
import com.example.khalifa.task.view.interfaces.BaseView;
import com.example.khalifa.task.view.interfaces.BaseViewSubscriber;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public abstract class BasePresenter<V extends BaseView, I extends BaseInterActor> {
    private V view;
    private I interActor;
    private CompositeDisposable subscriptions;

    protected I getInterActor() {
        return interActor;
    }

    protected abstract I initInterActor();

    public BasePresenter(V view) {
        this.view = view;
        subscriptions = new CompositeDisposable();
        interActor = initInterActor();
    }

    protected V getView() {
        return view;
    }

    public abstract void onCreate();

    public abstract void onStart();

    public abstract void onResume();

    public abstract void onPause();

    public abstract void onStop();


    public void onDestroy() {
        unsubscribeAll();
        view = null;
    }

    protected <T extends Object> Disposable subscribeObservable(
            BaseViewSubscriber<T> baseViewSubscriber,
            Observable<T> observable) {
        return baseViewSubscriber.subscribeObservable(observable);
    }


    private void unsubscribeAll() {
        subscriptions.dispose();
    }

    protected void addSubscribe(Disposable subscriber) {
        subscriptions.add(subscriber);
    }

    protected boolean isViewAttached() {
        return view != null;
    }

}