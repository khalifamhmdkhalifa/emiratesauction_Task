package com.example.khalifa.task.model.response;

import com.example.khalifa.task.model.entity.Car;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarsListResponse {
    private String alertEn;
    private String alertAr;
    @SerializedName("RefreshInterval")
    private int refreshInterval;
    @SerializedName("Ticks")
    private String ticks;
    private int count;
    private long endDate;
    private String sortOption;
    private String sortDirection;
    @SerializedName("Cars")
    private List<Car> carsList;

    public String getAlertEn() {
        return alertEn;
    }

    public String getAlertAr() {
        return alertAr;
    }

    public int getRefreshInterval() {
        return refreshInterval;
    }

    public String getTicks() {
        return ticks;
    }

    public int getCount() {
        return count;
    }

    public long getEndDate() {
        return endDate;
    }

    public String getSortOption() {
        return sortOption;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public List<Car> getCarsList() {
        return carsList;
    }
}
