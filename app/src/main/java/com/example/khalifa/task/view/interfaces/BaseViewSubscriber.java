package com.example.khalifa.task.view.interfaces;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public abstract class BaseViewSubscriber<T extends Object> {
    private final boolean showNoInternetView;
    protected BaseView view;
    protected boolean showLoading;

    public Disposable subscribeObservable(Observable<T> observable) {
        return observable.subscribe(new Consumer<T>() {
            @Override
            public void accept(T result) {
                if (showLoading)
                    view.hideLoading();
                onSuccess(result);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                if (showLoading)
                    view.hideLoading();
                onError(throwable);
            }
        }, new Action() {
            @Override
            public void run() throws Exception {
                if (showLoading)
                    view.showLoading();
            }
        });
    }

    public BaseViewSubscriber(BaseView view,
                              boolean showLoading, boolean showNoInternetView) {
        this.view = view;
        this.showLoading = showLoading;
        this.showNoInternetView = showNoInternetView;
    }

    protected void onError(Throwable throwable) {
        throwable.printStackTrace();
        if (showLoading)
            view.hideLoading();
    }

    public abstract void onSuccess(T result);
}