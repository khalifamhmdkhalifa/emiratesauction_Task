package com.example.khalifa.task.view.interfaces;

public interface InternetView extends BaseView {
    void showNoInternetView();

    void hideNoInternetView();
}
