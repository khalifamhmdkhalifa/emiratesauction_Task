package com.example.khalifa.task.view.interfaces;

import android.content.Context;

public interface BaseView {
    Context getContext();

    void showLoading();

    void hideLoading();
}