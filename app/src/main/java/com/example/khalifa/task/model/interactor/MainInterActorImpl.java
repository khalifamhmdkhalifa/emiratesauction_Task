package com.example.khalifa.task.model.interactor;

import com.example.khalifa.task.model.api.CarsApi;
import com.example.khalifa.task.model.interactor.interfaces.MainInterActor;
import com.example.khalifa.task.model.response.CarsListResponse;

import io.reactivex.Observable;

public class MainInterActorImpl extends MainInterActor {


    @Override
    public Observable<CarsListResponse> getCars(long ticks) {
        return applyScheduler(getRetrofitInstance().create(CarsApi.class).getCarsList(ticks));
    }
}
