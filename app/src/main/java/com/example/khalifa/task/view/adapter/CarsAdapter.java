package com.example.khalifa.task.view.adapter;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.khalifa.task.R;
import com.example.khalifa.task.model.entity.Car;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarsAdapter extends BaseRecyclerViewAdapter<Car, CarsAdapter.Holder> {
    private static final long SECONDS_IN_MINUTE = 60;
    private static final long SECONDS_IN_HOUR = SECONDS_IN_MINUTE * 60;
    private static final long TIME_LIMIT = SECONDS_IN_MINUTE * 5;
    private final String HEIGHT_KEY = "[h]";
    private final String WIDTH_KEY = "[w]";
    private final String HEIGHT_VALUE;
    private final String WIDTH_VALUE;
    private final int IMAGE_DP = 110;

    public CarsAdapter(Context context, List<Car> items) {
        super(context, items);
        WIDTH_VALUE = String.valueOf(dpToPx(IMAGE_DP));
        HEIGHT_VALUE = String.valueOf(dpToPx(IMAGE_DP));

    }


    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    int getItemLayoutId(int viewType) {
        return R.layout.list_item_car;
    }

    @Override
    Holder createHolder(View rootView, int viewType) {
        return new Holder(rootView);
    }

    @Override
    void onBindViewHolder(Holder holder, Car item, int position) {
        if (item.getImage() != null && !item.getImage().isEmpty())
            Glide.with(getContext()).load(
                    item.getImage().replace(HEIGHT_KEY, HEIGHT_VALUE).replace(
                            WIDTH_KEY, WIDTH_VALUE)).into(holder.carImage);
        else
            holder.carImage.setImageResource(R.drawable.ic_launcher_background);
        holder.bids.setText(String.valueOf(item.getAuctionInfo().getBids()));
        holder.lootNumber.setText(String.valueOf(item.getAuctionInfo().getLot()));
        holder.price.setText(String.valueOf(item.getAuctionInfo().getCurrentPrice()));
        if (isArabicLanguageSelected())
            setArabicData(holder, item);
        else
            setEnglishData(holder, item);

        setDateValue(holder, item);
    }

    private void setDateValue(Holder holder, Car item) {
        long totalSeconds = item.getAuctionInfo().getEndDate();

        if (totalSeconds <= TIME_LIMIT)
            holder.timeLeft.setTextColor(getContext().getResources().getColor(R.color.red));
        else
            holder.timeLeft.setTextColor(
                    getContext().getResources().getColor(R.color.color_000000));
        int hours = (int) (totalSeconds / SECONDS_IN_HOUR);
        totalSeconds = totalSeconds - SECONDS_IN_HOUR * hours;
        int minutes = (int) ((totalSeconds) / SECONDS_IN_MINUTE);
        int seconds = (int) (totalSeconds - (minutes * SECONDS_IN_MINUTE));
        holder.timeLeft.setText(String.format("%02d:%02d:%02d", hours, minutes, seconds));
    }

    private boolean isArabicLanguageSelected() {
        return getContext().getResources().getConfiguration().
                getLayoutDirection() == Configuration.SCREENLAYOUT_LAYOUTDIR_RTL;
    }

    private void setEnglishData(Holder holder, Car item) {
        holder.currency.setText(item.getAuctionInfo().getCurrencyEn());
        holder.name.setText(item.getMakeEn() + " " + item.getBodyEn() + item.getYear());
    }

    private void setArabicData(Holder holder, Car item) {
        holder.currency.setText(item.getAuctionInfo().getCurrencyAr());
        holder.name.setText(item.getMakeAr() + " " + item.getBodyAr() + item.getYear());
    }

    @Override
    void onItemClicked(Car item, int position) {
    }


    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView_timeLeft)
        TextView timeLeft;
        @BindView(R.id.textView_bids)
        TextView bids;
        @BindView(R.id.textView_carName)
        TextView name;
        @BindView(R.id.imageView_carImage)
        ImageView carImage;
        @BindView(R.id.textView_currency)
        TextView currency;
        @BindView(R.id.textView_lootNumber)
        TextView lootNumber;
        @BindView(R.id.textView_price)
        TextView price;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}