package com.example.khalifa.task.model.api;

import com.example.khalifa.task.model.response.CarsListResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CarsApi {
    @GET("carsonline")
    Observable<CarsListResponse> getCarsList(@Query("Ticks") long ticks);
}
