package com.example.khalifa.task.presenter;

import com.example.khalifa.task.model.entity.Car;
import com.example.khalifa.task.model.interactor.MainInterActorImpl;
import com.example.khalifa.task.model.interactor.interfaces.MainInterActor;
import com.example.khalifa.task.model.response.CarsListResponse;
import com.example.khalifa.task.presenter.interfaces.MainPresenter;
import com.example.khalifa.task.view.interfaces.BaseViewSubscriber;
import com.example.khalifa.task.view.interfaces.MainView;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainPresenterImpl extends MainPresenter {
    private List<Car> carList;
    private Timer requestsTimer;
    private Timer ticksTimer;

    private int refreshInterval;
    private String lastTicks;

    public MainPresenterImpl(MainView view) {
        super(view);
    }

    @Override
    public void onCarsSwipeRefresh() {
        reloadCarsList();
    }

    @Override
    protected MainInterActor initInterActor() {
        return new MainInterActorImpl();
    }

    @Override
    public void onCreate() {
        loadCars();
    }

    private void loadCars() {
        addSubscribe(subscribeObservable(new BaseViewSubscriber<CarsListResponse>(
                getView(), true, false) {
            @Override
            public void onSuccess(CarsListResponse result) {
                handleSuccessfulResponse(result);
                if (ticksTimer == null)
                    startTicksTimer();
            }
        }, getInterActor().getCars(0)));
    }

    private void startTicksTimer() {
        ticksTimer = new Timer();
        ticksTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isViewAttached() &&
                        carList != null && !carList.isEmpty()) {
                    for (Car car : carList)
                        car.getAuctionInfo().setEndDate(car.getAuctionInfo().getEndDate() - 1);
                    getView().notifyCarsListChanged();
                }
            }
        }, 1000, 1000);
    }

    private void handleSuccessfulResponse(CarsListResponse result) {
        refreshInterval = result.getRefreshInterval();
        lastTicks = result.getTicks();
        carList = result.getCarsList();
        getView().setupCarsRecyclerView(carList);
        startTimer(refreshInterval);
    }

    private void startTimer(int refreshInterval) {
        requestsTimer = new Timer();
        requestsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                refreshCarsList();
            }
        }, refreshInterval, refreshInterval);
    }

    private void refreshCarsList() {
        addSubscribe(subscribeObservable(new BaseViewSubscriber<CarsListResponse>(
                getView(), false, false) {
            @Override
            public void onSuccess(CarsListResponse result) {
                handleSuccessfulRefreshResponse(result);
                if (ticksTimer == null)
                    startTicksTimer();

            }
        }, getInterActor().getCars(Long.parseLong(lastTicks))));

    }

    private void handleSuccessfulRefreshResponse(CarsListResponse result) {
        lastTicks = result.getTicks();
        if (carList == null || carList.isEmpty()) {
            carList = result.getCarsList();
            getView().setupCarsRecyclerView(carList);
        } else {
            updateCarsList(result.getCarsList());
            getView().notifyCarsListChanged();
        }
    }

    private void updateCarsList(List<Car> updatedCarsList) {
        boolean anyUpdates = false;
        for (Car car : updatedCarsList) {
            if (updateCar(car, carList))
                anyUpdates = true;
        }
        if (anyUpdates)
            getView().notifyCarsListChanged();

    }

    private boolean updateCar(Car car, List<Car> carList) {
        for (Car currentCar : carList)
            if (currentCar.getCarId() == car.getCarId()) {
                carList.set(carList.indexOf(currentCar), car);
                return true;
            }
        return false;
    }

    private void reloadCarsList() {
        addSubscribe(subscribeObservable(new BaseViewSubscriber<CarsListResponse>(
                getView(), false, false) {
            @Override
            public void onSuccess(CarsListResponse result) {

                handleSuccessfulReloadResponse(result);
                if (isViewAttached())
                    getView().stopCarListRefreshing();
            }

            @Override
            protected void onError(Throwable throwable) {
                super.onError(throwable);
                if (isViewAttached())
                    getView().stopCarListRefreshing();

            }
        }, getInterActor().getCars(0)));


    }

    private void handleSuccessfulReloadResponse(CarsListResponse result) {
        if (result == null || result.getCarsList() == null
                || result.getCarsList().isEmpty())
            return;

        if (carList != null) {
            carList.clear();
            getView().notifyCarsListChanged();
            carList.addAll(result.getCarsList());
            getView().notifyCarsListChanged();
        } else {
            carList = result.getCarsList();
            getView().setupCarsRecyclerView(carList);
        }
        getView().notifyCarsListChanged();
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (ticksTimer != null)
            ticksTimer.cancel();
        if (requestsTimer != null)
            requestsTimer.cancel();
    }
}
