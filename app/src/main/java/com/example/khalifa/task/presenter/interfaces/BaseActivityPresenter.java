package com.example.khalifa.task.presenter.interfaces;


import com.example.khalifa.task.model.interactor.interfaces.BaseInterActor;
import com.example.khalifa.task.view.interfaces.BaseActivityView;

public abstract class BaseActivityPresenter <V extends BaseActivityView,I extends BaseInterActor>
        extends BasePresenter<V,I> {


    public BaseActivityPresenter(V view) {
        super(view);
    }
}