package com.example.khalifa.task.presenter.interfaces;

import com.example.khalifa.task.model.interactor.interfaces.MainInterActor;
import com.example.khalifa.task.view.interfaces.MainView;

public abstract class MainPresenter extends BaseActivityPresenter<MainView, MainInterActor> {

    public MainPresenter(MainView view) {
        super(view);
    }

    public abstract void onCarsSwipeRefresh();
}
