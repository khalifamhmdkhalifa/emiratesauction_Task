package com.example.khalifa.task.view.interfaces;

import com.example.khalifa.task.model.entity.Car;

import java.util.List;

public interface MainView extends BaseActivityView {
    void setupCarsRecyclerView(List<Car> carList);

    void notifyCarsListChanged();

    void stopCarListRefreshing();}
