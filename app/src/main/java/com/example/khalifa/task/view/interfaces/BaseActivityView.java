package com.example.khalifa.task.view.interfaces;

public interface BaseActivityView extends BaseView, InternetView {
    void finishActivity();
}
