package com.example.khalifa.task;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.khalifa.task.model.entity.Car;
import com.example.khalifa.task.presenter.MainPresenterImpl;
import com.example.khalifa.task.presenter.interfaces.MainPresenter;
import com.example.khalifa.task.view.adapter.BaseRecyclerViewAdapter;
import com.example.khalifa.task.view.adapter.CarsAdapter;
import com.example.khalifa.task.view.interfaces.BaseActivityImplement;
import com.example.khalifa.task.view.interfaces.MainView;

import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivityImplement<MainPresenter> implements MainView {

    @BindView(R.id.recyclerView_cars)
    RecyclerView recyclerViewCars;
    @BindView(R.id.SwipeRefresh_cars)
    SwipeRefreshLayout swipeRefreshLayoutCars;
    private BaseRecyclerViewAdapter carsAdapter;

    @Override
    public MainPresenter initializePresenter() {
        return new MainPresenterImpl(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(this, "Hello", Toast.LENGTH_LONG).show();
        swipeRefreshLayoutCars.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPresenter().onCarsSwipeRefresh();

            }
        });
    }

    @Override
    protected int getNoInternetViewId() {
        return 0;
    }

    @Override
    protected int getLoadingViewId() {
        return R.layout.layout_loading;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void setupCarsRecyclerView(List<Car> carList) {
        recyclerViewCars.setLayoutManager(new LinearLayoutManager(getContext()));
        carsAdapter = new CarsAdapter(getContext(), carList);
        recyclerViewCars.setAdapter(carsAdapter);
    }

    @Override
    public void notifyCarsListChanged() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (carsAdapter != null)
                    carsAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void stopCarListRefreshing() {
        swipeRefreshLayoutCars.setRefreshing(false);
    }
}
