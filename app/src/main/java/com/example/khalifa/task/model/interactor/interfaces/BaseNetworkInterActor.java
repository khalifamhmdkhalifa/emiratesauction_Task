package com.example.khalifa.task.model.interactor.interfaces;


import com.example.khalifa.task.model.interactor.RetrofitManager;

import retrofit2.Retrofit;

public abstract class BaseNetworkInterActor extends BaseInterActor {
    private Retrofit retrofit;

    public BaseNetworkInterActor() {
        retrofit = getRetroFit();
    }

    private Retrofit getRetroFit() {
        return RetrofitManager.getRetrofitInstance();
    }

    protected Retrofit getRetrofitInstance() {
        return retrofit;
    }
}