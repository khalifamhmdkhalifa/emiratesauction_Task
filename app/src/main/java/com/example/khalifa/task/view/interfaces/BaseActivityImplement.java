package com.example.khalifa.task.view.interfaces;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


import com.example.khalifa.task.presenter.interfaces.BaseActivityPresenter;

import butterknife.ButterKnife;

public abstract class BaseActivityImplement<P extends BaseActivityPresenter> extends AppCompatActivity
        implements BaseActivityView {
    private P presenter;
    private View loadingView;
    private View noInternetView;


    protected P getPresenter() {
        return presenter;
    }

    public abstract P initializePresenter();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        loadingView = findViewById(getLoadingViewId());
        noInternetView = findViewById(getNoInternetViewId());
        presenter = initializePresenter();
        getPresenter().onCreate();
    }

    protected abstract int getNoInternetViewId();

    protected abstract int getLoadingViewId();

    protected abstract int getLayoutId();

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getPresenter().onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().onDestroy();

    }


    @Override
    protected void onPause() {
        super.onPause();
        getPresenter().onPause();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        if (loadingView != null)
            loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        if (loadingView != null)
            loadingView.setVisibility(View.GONE);
    }


    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void showNoInternetView() {
        if (noInternetView != null)
            noInternetView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoInternetView() {
        if (noInternetView != null)
            noInternetView.setVisibility(View.GONE);
    }
}