package com.example.khalifa.task.presenter.interfaces;


import com.example.khalifa.task.model.interactor.interfaces.BaseInterActor;
import com.example.khalifa.task.view.interfaces.BaseFragmentView;

public abstract class BaseFragmentPresenter<V extends BaseFragmentView, I extends BaseInterActor>
        extends BasePresenter<V, I> {
    public BaseFragmentPresenter(V view) {
        super(view);
    }
}
